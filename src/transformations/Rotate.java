package transformations;

import java.awt.*;
import java.text.ParseException;

public class Rotate extends Transformation
{
    private int degree_;

    public Rotate(int _degree)
    {
        degree_ = _degree;
    }

    public Rotate(String _string) throws ParseException
    {
        if (!_string.contains("Rotate("))
            throw new ParseException("Invalid string", 0);

        degree_ = Integer.parseInt(_string.substring(7, _string.length() - 1));
    }

    @Override
    public Point transform(Point _point, Point _gravity)
    {
        Matrix pointMatrix = new Matrix(1, 3);
        pointMatrix.set(0, 0, _point.x);
        pointMatrix.set(0, 1, _point.y);
        pointMatrix.set(0, 2, 1);

        Matrix moveMatrix = new Matrix(3, 3);
        moveMatrix.set(0, 0, 1);
        moveMatrix.set(1, 1, 1);
        moveMatrix.set(2, 2, 1);
        moveMatrix.set(2, 0, -_gravity.x);
        moveMatrix.set(2, 1, -_gravity.y);

        Matrix result = pointMatrix.multiply(moveMatrix);

        Matrix rotateMatrix = new Matrix(3, 3);
        double cosinus = Math.cos(Math.toRadians(degree_));
        double sinus = Math.sin(Math.toRadians(degree_));
        rotateMatrix.set(0, 0, cosinus);
        rotateMatrix.set(1, 1, cosinus);
        rotateMatrix.set(0, 1, sinus);
        rotateMatrix.set(1, 0, -sinus);
        rotateMatrix.set(2, 2, 1);

        result = result.multiply(rotateMatrix);

        moveMatrix.set(2, 0, _gravity.x);
        moveMatrix.set(2, 1, _gravity.y);

        result = result.multiply(moveMatrix);
        return new Point((int)result.get(0, 0), (int)result.get(0, 1));
    }

    @Override
    public String toString()
    {
        return "Rotate(" + degree_ + ")";
    }
}