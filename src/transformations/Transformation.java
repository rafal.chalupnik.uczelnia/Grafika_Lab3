package transformations;

import java.awt.*;

public abstract class Transformation
{
    public abstract Point transform(Point _point, Point _gravity);
}