package transformations;

import java.awt.*;
import java.text.ParseException;

public class Scale extends Transformation
{
    private double xScale_;
    private double yScale_;

    public Scale(double _xScale, double _yScale)
    {
        if (_xScale <= 0 || _yScale <= 0)
            throw new IllegalArgumentException("Wrong parameters!");

        xScale_ = _xScale;
        yScale_ = _yScale;
    }

    public Scale(String _string) throws ParseException
    {
        if (!_string.contains("Scale("))
            throw new ParseException("Invalid string", 0);

        String data = _string.substring(6, _string.length() - 1);
        String[] xyScale = data.split(",");

        if (xyScale.length != 2)
            throw new ParseException("Invalid data", 0);

        xScale_ = Double.parseDouble(xyScale[0]);
        yScale_ = Double.parseDouble(xyScale[1]);
    }

    @Override
    public String toString()
    {
        return "Scale(" + xScale_ + "," + yScale_ + ")";
    }

    @Override
    public Point transform(Point _point, Point _gravity)
    {
        Matrix pointMatrix = new Matrix(1, 3);
        pointMatrix.set(0, 0, _point.x);
        pointMatrix.set(0, 1, _point.y);
        pointMatrix.set(0, 2, 1);

        Matrix moveMatrix = new Matrix(3, 3);
        moveMatrix.set(0, 0, 1);
        moveMatrix.set(1, 1, 1);
        moveMatrix.set(2, 2, 1);
        moveMatrix.set(2, 0, -_gravity.x);
        moveMatrix.set(2, 1, -_gravity.y);

        Matrix result = pointMatrix.multiply(moveMatrix);

        Matrix scaleMatrix = new Matrix(3, 3);
        scaleMatrix.set(0, 0, xScale_);
        scaleMatrix.set(1, 1, yScale_);
        scaleMatrix.set(2, 2, 1);

        result = result.multiply(scaleMatrix);

        moveMatrix.set(2, 0, _gravity.x);
        moveMatrix.set(2, 1, _gravity.y);

        result = result.multiply(moveMatrix);
        return new Point((int)result.get(0, 0), (int)result.get(0, 1));
    }
}