package transformations;

public class Matrix
{
    private double[][] matrix_ = null;

    public Matrix(int _rows, int _columns)
    {
        matrix_ = new double[_rows][_columns];
    }

    public double get(int _row, int _column)
    {
        return matrix_[_row][_column];
    }

    public int getColumns() { return matrix_[0].length; }
    public int getRows() { return matrix_.length; }

    public void set(int _row, int _column, double _value)
    {
        matrix_[_row][_column] = _value;
    }

    public Matrix multiply(Matrix _matrix)
    {
        if (getColumns() != _matrix.getRows())
            throw new RuntimeException("Dimensions are invalid!");

        Matrix output = new Matrix(getRows(), _matrix.getColumns());

        for (int row = 0; row < output.getRows(); row++)
        {
            for (int column = 0; column < output.getColumns(); column++)
            {
                for (int i = 0; i < getColumns(); i++)
                {
                    output.set(row, column, output.get(row, column) + matrix_[row][i] * _matrix.get(i, column));
                }
            }
        }

        return output;
    }

    @Override
    public String toString()
    {
        String output = "";
        for (int row = 0; row < matrix_.length; row++)
        {
            for (int column = 0; column < matrix_[0].length; column++)
            {
                output += String.valueOf(matrix_[row][column]) + "\t";
            }
            output += "\n";
        }

        return output;
    }
}