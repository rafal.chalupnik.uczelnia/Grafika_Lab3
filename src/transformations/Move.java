package transformations;

import java.awt.*;
import java.text.ParseException;

public class Move extends Transformation
{
    private int x_;
    private int y_;

    public Move(int _x, int _y)
    {
        x_ = _x;
        y_ = _y;
    }

    public Move(String _string) throws ParseException
    {
        if (!_string.contains("Move("))
            throw new ParseException("Invalid string!", 0);

        String data = _string.substring(5, _string.length() - 1);
        String[] coordinates = data.split(",");
        if (coordinates.length != 2)
            throw new ParseException("Invalid number of coordinates!", 0);

        x_ = Integer.parseInt(coordinates[0]);
        y_ = Integer.parseInt(coordinates[1]);
    }

    @Override
    public String toString()
    {
        return "Move(" + x_ + "," + y_ + ")";
    }

    @Override
    public Point transform(Point _point, Point _gravity)
    {
        Matrix pointMatrix = new Matrix(1, 3);
        pointMatrix.set(0, 0, _point.x);
        pointMatrix.set(0, 1, _point.y);
        pointMatrix.set(0, 2, 1);

        Matrix moveMatrix = new Matrix(3, 3);
        moveMatrix.set(0, 0, 1);
        moveMatrix.set(1, 1, 1);
        moveMatrix.set(2, 2, 1);
        moveMatrix.set(2, 0, x_);
        moveMatrix.set(2, 1, y_);

        Matrix result = pointMatrix.multiply(moveMatrix);
        return new Point((int)result.get(0, 0), (int)result.get(0, 1));
    }
}