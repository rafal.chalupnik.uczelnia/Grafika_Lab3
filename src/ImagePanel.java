import shapes.Shape;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class ImagePanel extends JPanel
{
    private ArrayList<Shape> shapes_ = new ArrayList<>();

    private ArrayList<BufferedImage> images_ = new ArrayList<BufferedImage>();

    public void setImages(ArrayList<Shape> _shapes)
    {
        shapes_ = _shapes;
    }

    public void addImage(BufferedImage _image)
    {
        images_.add(_image);
    }

    @Override
    protected void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        for (Shape shape : shapes_)
            shape.draw(g);
        for (BufferedImage image : images_)
            g.drawImage(image, 0, 0, this);
    }
}