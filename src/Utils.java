import shapes.*;
import shapes.Polygon;
import shapes.Shape;
import transformations.Move;
import transformations.Rotate;
import transformations.Scale;
import transformations.Transformation;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.ParseException;
import java.util.ArrayList;

public class Utils
{
    public static shapes.Shape generateShape(String _string) throws ParseException
    {
        if (_string.contains("Stretch"))
            return new Stretch(_string);
        else if (_string.contains("Polygon"))
            return new Polygon(_string);
        else if (_string.contains("Circle"))
            return new Circle(_string);
        else return null;
    }

    public static Transformation generateTransformation(String _string) throws ParseException
    {
        if (_string.contains("Move"))
            return new Move(_string);
        else if (_string.contains("Rotate"))
            return new Rotate(_string);
        else if (_string.contains("Scale"))
            return new Scale(_string);
        else throw new ParseException("Transformation is not recognized", 0);
    }

    public static ArrayList<Shape> readShapesFromFile(String _path) throws IOException, ParseException
    {
        BufferedReader reader = new BufferedReader(new FileReader(_path));
        ArrayList<Shape> shapes = new ArrayList<>();
        String line;

        while ((line = reader.readLine()) != null)
            shapes.add(generateShape(line));

        reader.close();
        return shapes;
    }

    public static void writeShapesToFile(String _path, ArrayList<Shape> _shapes) throws IOException
    {
        BufferedWriter writer = new BufferedWriter(new FileWriter(_path));

        for (shapes.Shape shape : _shapes)
        {
            writer.write(shape.toString());
            writer.newLine();
        }

        writer.close();
    }

    public static ArrayList<Transformation> readTransformationsFromFile(String _path) throws IOException, ParseException
    {
        BufferedReader reader = new BufferedReader(new FileReader(_path));
        ArrayList<Transformation> transformations = new ArrayList<>();
        String line;

        while ((line = reader.readLine()) != null)
            transformations.add(generateTransformation(line));

        reader.close();
        return transformations;
    }

    public static void writeTransformationsToFile(String _path, ArrayList<Transformation> _transformations) throws IOException
    {
        BufferedWriter writer = new BufferedWriter(new FileWriter(_path));

        for (Transformation transformation : _transformations)
        {
            writer.write(transformation.toString());
            writer.newLine();
        }

        writer.close();
    }

    public static void displayShapes(ArrayList<Shape> _shapes, int _width, int _height)
    {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setPreferredSize(new Dimension(_width, _height));

        ImagePanel panel = new ImagePanel();
        panel.setSize(_width, _height);
        panel.setImages(_shapes);

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    public static void displayImage(BufferedImage _image)
    {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setPreferredSize(new Dimension(_image.getWidth(), _image.getHeight()));

        ImagePanel panel = new ImagePanel();
        panel.setSize(_image.getWidth(), _image.getHeight());
        panel.addImage(_image);

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
    }

    public static BufferedImage rotateRasterImage(BufferedImage _image, int _degree)
    {
        AffineTransform tx = new AffineTransform();
        tx.translate(_image.getWidth() / 2,_image.getHeight() / 2);
        tx.rotate(Math.toRadians(_degree));
        tx.translate(- _image.getWidth() / 2,- _image.getHeight() / 2);

        AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);

        BufferedImage output = new BufferedImage(_image.getWidth(), _image.getHeight(), _image.getType());
        op.filter(_image, output);

        return output;
    }

    public static BufferedImage readImageFromFile(File _file) throws IOException
    {
        return ImageIO.read(_file);
    }
}