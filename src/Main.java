import shapes.Shape;
import transformations.Transformation;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.util.ArrayList;

public class Main
{
    public static void main(String[] args) throws ParseException
    {
        printHeader();
        cmdInteface();
    }

    public static void printHeader()
    {
        System.out.println("#######################");
        System.out.println("#### Grafika Lab 3 ####");
        System.out.println("### Rafał Chałupnik ###");
        System.out.println("#######################");
        System.out.println("");
        System.out.println("Type command:");
    }

    static ArrayList<Shape> shapeList = new ArrayList<>();
    static ArrayList<Transformation> transformationList = new ArrayList<>();

    public static void cmdInteface()
    {
        boolean isRun = true;
        String command;
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (isRun)
        {
            try
            {
                command = br.readLine();
            }
            catch (Exception e)
            {
                System.out.println("Error in console input:");
                System.out.println(e);
                break;
            }

            if (command.equals("exit"))
                isRun = false;
            else if (command.equals("help"))
            {
                System.out.println("Available options:\n");
                System.out.println("exit");
                System.out.println("help");
                System.out.println("show");
                System.out.println("transform");
                System.out.println("list shapes");
                System.out.println("list transformations");
                System.out.println("clear");
                System.out.println("clear shapes");
                System.out.println("clear transformations");
                System.out.println("load shapes <path>");
                System.out.println("load transformations <path>");
                System.out.println("save <path>");
                System.out.println("Circle(x,y;radius)");
                System.out.println("Polygon(x1,y1;x2,y2;...)");
                System.out.println("Stretch(x1,y1;x2,y2)");
                System.out.println("Move(x,y)");
                System.out.println("Rotate(degree)");
                System.out.println("Scale(x,y)");
            }
            else if (command.contains("raster"))
            {
                String[] data = command.split("\\s+");
                if (data.length != 2)
                    System.out.println("Invalid command!");

                try
                {
                    BufferedImage image = Utils.readImageFromFile(new File(data[1]));
                    Utils.displayImage(image);
                    BufferedImage transformed = Utils.rotateRasterImage(image, 45);
                    Utils.displayImage(transformed);
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }
            }
            else if (command.equals("show"))
                Utils.displayShapes(shapeList, 1024, 768);
            else if (command.equals("transform"))
            {
                ArrayList<Shape> transformedShapeList = new ArrayList<>();
                for (Shape shape : shapeList)
                {
                    for (Transformation transformation : transformationList)
                    {
                        shape = shape.transform(transformation);
                    }
                    transformedShapeList.add(shape);
                }
                shapeList = transformedShapeList;
                transformationList.clear();
                System.out.println("Transformation complete.");
            }
            else if (command.contains("list"))
            {
                if (command.contains("shapes"))
                {
                    for (Shape shape : shapeList)
                        System.out.println(shape.toString());
                }
                else if (command.contains("transformations"))
                {
                    for (Transformation transformation : transformationList)
                        System.out.println(transformation.toString());
                }
                else
                    System.out.println("Invalid command!");
            }
            else if (command.contains("clear"))
            {
                if (command.contains("shapes"))
                    shapeList.clear();
                else if (command.contains("transformations"))
                    transformationList.clear();
                else
                {
                    shapeList.clear();
                    transformationList.clear();
                }
                System.out.println("Clear complete.");
            }
            else if (command.contains("load"))
            {
                String[] data = command.split("\\s+");
                if (data.length != 3)
                    System.out.println("Invalid command!");

                try
                {
                    if (command.contains("shapes"))
                    {
                        shapeList = Utils.readShapesFromFile(data[2]);
                        System.out.println("Load complete.");
                    }
                    else if (command.contains("transformations"))
                    {
                        transformationList = Utils.readTransformationsFromFile(data[2]);
                        System.out.println("Load complete.");
                    }
                    else
                        System.out.println("Invalid command!");
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }
            }
            else if (command.contains("save"))
            {
                String[] data = command.split("\\s+");
                if (data.length != 2)
                    System.out.println("Invalid command!");

                try
                {
                    Utils.writeShapesToFile(data[1], shapeList);
                    System.out.println("Save complete.");
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }
            }
            else
            {
                try
                {
                    Shape shape = Utils.generateShape(command);
                    Transformation transformation;
                    if (shape != null)
                    {
                        shapeList.add(shape);
                        System.out.println(shape.toString() + " added.");
                    }
                    else
                    {
                        transformation = Utils.generateTransformation(command);
                        if (transformation != null)
                        {
                            transformationList.add(transformation);
                            System.out.println(transformation.toString() + " added.");
                        }
                        else
                            System.out.println("Invalid command!");
                    }
                }
                catch (Exception e)
                {
                    System.out.println(e);
                }
            }
        }
    }
}