package shapes;

import transformations.Transformation;

import java.awt.*;
import java.text.ParseException;
import java.util.ArrayList;

public class Stretch extends Shape
{
    private Point endPoint_;
    private Point startPoint_;

    public Stretch(Point _startPoint, Point _endPoint)
    {
        startPoint_ = _startPoint;
        endPoint_ = _endPoint;
        gravityCenter_ = new Point((endPoint_.x - startPoint_.x) / 2, (endPoint_.y - startPoint_.y) / 2);
    }

    public Stretch(String _string) throws ParseException
    {
        if (!_string.contains("Stretch("))
            throw new ParseException("Invalid Stretch string.", 0);

        ArrayList<Point> points = extractPoints("Stretch(", _string);
        if (points.size() != 2)
            throw new ParseException("Invalid number of points.", 0);

        startPoint_ = points.get(0);
        endPoint_ = points.get(1);
        gravityCenter_ = new Point((endPoint_.x - startPoint_.x) / 2, (endPoint_.y - startPoint_.y) / 2);
    }

    @Override
    public void draw(Graphics _image)
    {
        _image.drawLine(startPoint_.x, startPoint_.y, endPoint_.x, endPoint_.y);
    }

    @Override
    public String toString()
    {
        return "Stretch(" + startPoint_.x + "," + startPoint_.y + ";" + endPoint_.x + "," + endPoint_.y + ")";
    }

    @Override
    public Shape transform(Transformation _transformation)
    {
        Point newStart = _transformation.transform(startPoint_, gravityCenter_);
        Point newEnd = _transformation.transform(endPoint_, gravityCenter_);

        return new Stretch(newStart, newEnd);
    }
}