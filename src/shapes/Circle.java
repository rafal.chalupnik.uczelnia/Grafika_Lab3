package shapes;

import transformations.Rotate;
import transformations.Scale;
import transformations.Transformation;

import java.awt.*;
import java.text.ParseException;

public class Circle extends Shape
{
    private Point center_;
    private int radius_;

    public Circle(Point _center, int _radius)
    {
        center_ = _center;
        radius_ = _radius;
        gravityCenter_ = center_;
    }

    public Circle(String _string) throws ParseException
    {
        if (!_string.contains("Circle("))
            throw new ParseException("Invalid Circle string.", 0);

        _string = _string.substring(7, _string.length() - 1);

        String[] data = _string.split(";");
        if (data.length != 2)
            throw new ParseException("Invalid Circle string.", 0);

        String[] xy = data[0].split(",");
        if (xy.length != 2)
            throw new ParseException("Invalid point.", 0);

        center_ = new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1]));
        radius_ = Integer.parseInt(data[1]);
        gravityCenter_ = center_;
    }

    @Override
    public void draw(Graphics _image)
    {
        _image.drawOval(center_.x - radius_, center_.y - radius_, 2 * radius_, 2 * radius_);
    }

    @Override
    public String toString()
    {
        return "Circle(" + center_.x + "," + center_.y + ";" + radius_ + ")";
    }

    @Override
    public Shape transform(Transformation _transformation)
    {
        if (_transformation instanceof Rotate)
            return this;
        else if (_transformation instanceof Scale)
            return new Circle(center_, _transformation.transform(new Point(radius_, 0), new Point(0, 0)).x);
        else
            return new Circle(_transformation.transform(center_, gravityCenter_), radius_);
    }
}