package shapes;

import transformations.Transformation;

import java.awt.*;
import java.text.ParseException;
import java.util.ArrayList;

public class Polygon extends Shape
{
    ArrayList<Point> points_;

    private void updateGravityCenter()
    {
        int xSum = 0;
        int ySum = 0;

        for (Point point : points_)
        {
            xSum += point.x;
            ySum += point.y;
        }

        gravityCenter_ = new Point(xSum / points_.size(), ySum / points_.size());
    }

    public Polygon(Point... _points)
    {
        points_ = new ArrayList<>();
        for (Point point : _points)
            points_.add(point);
    }

    public Polygon(String _string) throws ParseException
    {
        if (!_string.contains("Polygon("))
            throw new ParseException("Invalid Polygon string.", 0);

        ArrayList<Point> points = extractPoints("Polygon(", _string);
        if (points.size() < 3)
            throw new ParseException("Invalid number of points.", 0);

        points_ = points;
        updateGravityCenter();
    }

    public void addPoint(Point _point)
    {
        points_.add(_point);
        updateGravityCenter();
    }

    @Override
    public void draw(Graphics _image)
    {
        int[] xPoints = new int[points_.size()];
        int[] yPoints = new int[points_.size()];

        for (int i = 0; i < xPoints.length; i++)
        {
            xPoints[i] = points_.get(i).x;
            yPoints[i] = points_.get(i).y;
        }

        _image.drawPolygon(xPoints, yPoints, xPoints.length);
    }

    @Override
    public String toString()
    {
        String output = "Polygon(";

        for (Point point : points_)
        {
            output += point.x + "," + point.y + ";";
        }

        return output.substring(0, output.length() - 1) + ")";
    }

    @Override
    public Shape transform(Transformation _transformation)
    {
        Polygon output = new Polygon();
        for (Point point : points_)
        {
            output.addPoint(_transformation.transform(point, gravityCenter_));
        }

        return output;
    }
}