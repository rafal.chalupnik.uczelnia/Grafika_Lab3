package shapes;

import transformations.Transformation;

import java.awt.*;
import java.text.ParseException;
import java.util.ArrayList;

public abstract class Shape
{
    public Point gravityCenter_;

    public abstract void draw(Graphics _image);
    public abstract String toString();

    public abstract Shape transform(Transformation _transformation);

    public static ArrayList<Point> extractPoints(String _prefix, String _string) throws ParseException
    {
        _string = _string.substring(_prefix.length(), _string.length() - 1);
        String[] coordinates = _string.split(";");
        ArrayList<Point> points = new ArrayList<>();

        for (String pointCoordinates : coordinates)
        {
            String[] xy = pointCoordinates.split(",");
            if (xy.length != 2)
                throw new ParseException("Invalid point!", 0);

            points.add(new Point(Integer.parseInt(xy[0]), Integer.parseInt(xy[1])));
        }

        return points;
    }
}